<?php
namespace Gmo\LanguageDetection\Tests;

use Gmo\LanguageDetection\NonLatinDetector;
use Gmo\LanguageDetection\Language;

/**
 * @group unit
 */
class NonLatinDetectorTest extends \PHPUnit_Framework_TestCase
{
    const ARABIC_TEXT = 'وهذا هو النص العربي';
    const CHINESE_TEXT = '这是中国文字';
    const RUSSIAN_TEXT = 'Это русский текст';

    public function test_arabic()
    {
        $detector = new NonLatinDetector();
        $language = $detector->detect(static::ARABIC_TEXT);

        $this->assertSame(Language::ARABIC, $language->getCode());
        $this->assertGreaterThan(0.49, $language->getConfidence());
    }

    public function test_chinese()
    {
        $detector = new NonLatinDetector();
        $language = $detector->detect(static::CHINESE_TEXT);

        $this->assertSame(Language::CHINESE, $language->getCode());
        $this->assertGreaterThan(0.9, $language->getConfidence());
    }

    public function test_russian()
    {
        $detector = new NonLatinDetector();
        $language = $detector->detect(static::RUSSIAN_TEXT);

        $this->assertSame(Language::RUSSIAN, $language->getCode());
        $this->assertGreaterThan(0.49, $language->getConfidence());
    }
}
