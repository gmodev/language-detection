<?php
namespace Gmo\LanguageDetection\Tests;

use Gmo\LanguageDetection\Detector;
use Gmo\LanguageDetection\Language;

/**
 * @group integration
 */
class DetectorTest extends \PHPUnit_Framework_TestCase
{
    const API_PATH = 'http://localhost:48000/';

    const ARABIC_TEXT = 'وهذا هو النص العربي';
    const CHINESE_TEXT = '这是中国文字';
    const RUSSIAN_TEXT = 'Это русский текст';
    const SPANISH_TEXT = 'Este es el texto español';
    const MIXED_ARABIC_AND_RUSSIAN_TEXT = 'Это ال русский текст';

    public function test_arabic()
    {
        $detector = new Detector(static::API_PATH);
        $language = $detector->detect(static::ARABIC_TEXT);

        $this->assertSame(Language::ARABIC, $language->getCode());
        $this->assertGreaterThan(0.49, $language->getConfidence());
    }

    public function test_chinese()
    {
        $detector = new Detector(static::API_PATH);
        $language = $detector->detect(static::CHINESE_TEXT);

        $this->assertSame(Language::CHINESE, $language->getCode());
        $this->assertGreaterThan(0.9, $language->getConfidence());
    }

    public function test_russian()
    {
        $detector = new Detector(static::API_PATH);
        $language = $detector->detect(static::RUSSIAN_TEXT);

        $this->assertSame(Language::RUSSIAN, $language->getCode());
        $this->assertGreaterThan(0.49, $language->getConfidence());
    }

    public function test_spanish()
    {
        $detector = new Detector(static::API_PATH);
        $language = $detector->detect(static::SPANISH_TEXT);

        $this->assertSame(Language::SPANISH, $language->getCode());
        $this->assertGreaterThan(0.9, $language->getConfidence());
    }

    public function test_mixed_arabic_and_russian()
    {
        $detector = new Detector(static::API_PATH);
        $language = $detector->detect(static::MIXED_ARABIC_AND_RUSSIAN_TEXT);

        $this->assertSame(Language::RUSSIAN, $language->getCode());
        $this->assertGreaterThan(0.45, $language->getConfidence());
    }
}
