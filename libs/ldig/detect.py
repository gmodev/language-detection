#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Command line utility that uses ldig

import sys, os, codecs
import urlparse
import optparse
import json
import numpy
import ldig
from pprint import pprint

#sys.stdout = codecs.getwriter('utf-8')(sys.stdout)

def predict(param, events):
    sum_w = numpy.dot(param[events.keys(),].T, events.values())
    exp_w = numpy.exp(sum_w - sum_w.max())
    return exp_w / exp_w.sum()

class Detector(object):
    def __init__(self, modeldir):
        self.ldig = ldig.ldig(modeldir)
        self.features = self.ldig.load_features()
        self.trie = self.ldig.load_da()
        self.labels = self.ldig.load_labels()
        self.param = numpy.load(self.ldig.param)

    def detect(self, st):
        labels = self.labels

        K = len(labels)
        corrects = numpy.zeros(K, dtype=int)
        counts = numpy.zeros(K, dtype=int)

        label_map = dict((x, i) for i, x in enumerate(labels))

        n_available_data = 0
        log_likely = 0.0
        label, text, org_text = ldig.normalize_text(st)
        label = "en"
        label_k = label_map[label]

        events = self.trie.extract_features(u"\u0001" + text + u"\u0001")
        y = predict(self.param, events)
        predict_k = y.argmax()

        if label_k >= 0:
            log_likely -= numpy.log(y[label_k])
            n_available_data += 1
            counts[label_k] += 1
            if label_k == predict_k and y[predict_k] >= 0.6:
                corrects[predict_k] += 1

        predict_lang = labels[predict_k]
        if y[predict_k] < 0.6: predict_lang = ""
        return {'detectedLanguage': predict_lang, 'confidence': y[predict_k]}

basedir = os.path.join(os.path.dirname(__file__), "static")

if __name__ == '__main__':
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    parser = optparse.OptionParser(usage='%prog -m model/directory "text to analyze"')
    parser.add_option("-m", dest="model", help="model directory")
    (options, args) = parser.parse_args()
    if not options.model: parser.error("need model directory (-m)")
    if not len(args) > 0: parser.error("need text to analyze")

    detector = Detector(options.model)
    detected_packet = detector.detect(args[0])
    print "Detected Language: %s" % (detected_packet['detectedLanguage'])
    print "Confidence: %f" % (detected_packet['confidence'])