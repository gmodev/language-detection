#!/bin/sh
#
# ldig - Language Detection with Infinity Gram
#
# chkconfig:   - 57 47
# description: Language Detection with Infinity Gram
# processname:  ldigd
#

# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network

LDIG_DIR="/usr/lib64/ldig"
prog=ldigd
exec="${LDIG_DIR}/${prog}"

PIDFILE="/var/run/${prog}.pid"

# default options
LDIGD_MODEL="${LDIG_DIR}/models/model.latin"

lockfile=/var/lock/subsys/ldigd

start() {
    [ -x $exec ] || exit 5
    echo -n $"Starting $prog: "

    options="-m ${LDIGD_MODEL}"

    daemon --pidfile="$PIDFILE" /usr/sbin/daemonize -p $PIDFILE $exec $options
    retval=$?
    echo
    [ $retval -eq 0 ] && touch $lockfile
    return $retval
}

stop() {
    echo -n $"Stopping $prog: "
    # stop it here, often "killproc $prog"
    killproc -p "$PIDFILE" $exec
    retval=$?
    echo
    [ $retval -eq 0 ] && rm -f $lockfile
    return $retval
}

restart() {
    stop
    start
}

reload() {
    restart
}

force_reload() {
    restart
}

rh_status() {
    # run checks to determine if the service is running or use generic status
    status -p "$PIDFILE" -l $prog $exec
}

rh_status_q() {
    rh_status >/dev/null 2>&1
}


case "$1" in
    start)
        rh_status_q && exit 0
        $1
        ;;
    stop)
        rh_status_q || exit 0
        $1
        ;;
    restart)
        $1
        ;;
    reload)
        rh_status_q || exit 7
        $1
        ;;
    force-reload)
        force_reload
        ;;
    status)
        rh_status
        ;;
    condrestart|try-restart)
        rh_status_q || exit 0
        restart
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart|try-restart|reload|force-reload}"
        exit 2
esac
exit $?

