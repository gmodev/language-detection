#!/usr/bin/env bash

#NOTE: Need to run this as root

FILE="$( readlink -f ${BASH_SOURCE[0]} )"
DIR="$( cd "$( dirname "${FILE}" )" && pwd -P )"
INSTALL_DIR="/usr/lib64/ldig"

if [ "$1" == "install" ]; then
	mkdir -p $INSTALL_DIR
	echo "Copying library files..."
	cd $DIR
	cp -r * $INSTALL_DIR
	cd $INSTALL_DIR
	chmod +x ldigd

	echo "Expanding model files..."
	cd $INSTALL_DIR/models
	tar xf model.latin.20120315.tar.xz
	tar -zxf ldig.model.small.tgz

	if [ "$2" == "upstart" ]; then
		echo "Setting up Upstart service management script..."
		cp $INSTALL_DIR/serviceManagement/upstart.conf /etc/init/ldigd.conf
		chown root:root /etc/init/ldigd.conf
		chmod 644 /etc/init/ldigd.conf

		echo "Starting ldigd..."
		sudo start ldigd
	else
		echo "Setting up SystemV service management script..."
		cp $INSTALL_DIR/serviceManagement/systemv.sh /etc/init.d/ldigd
		chown root:root /etc/init.d/ldigd
		chmod 755 /etc/init.d/ldigd

		echo "Starting ldigd..."
    	sudo /etc/init.d/ldigd start
    	sudo chkconfig ldigd on
	fi

	echo "Install complete!"
else
	echo "usage: ./ldig.sh install [upstart]"
	exit 1
fi
