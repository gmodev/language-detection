<?php
namespace Gmo\LanguageDetection;

use GuzzleHttp;

class LatinDetector
{
    /** @var GuzzleHttp\Client */
    protected $guzzle;

    public function __construct($apiPath = 'http://localhost:48000/')
    {
        $this->guzzle = new GuzzleHttp\Client(['base_uri' => $apiPath]);
    }

    /**
     * @param $text
     *
     * @return Language
     */
    public function detect($text)
    {
        $response = $this->guzzle->get('/', [
            'query' => [
                'text' => $text,
            ],
        ]);
        $json = json_decode($response->getBody(), true);

        return new Language($json['detectedLanguage'], $json['confidence']);
    }
}
