<?php
namespace Gmo\LanguageDetection;

class NonLatinDetector
{
    public function detect($text)
    {
        $nullLanguage = new Language(null, 0.0);

        $text = CharsetConverter::utf8ToNcr($text);
        $text = $this->latin1TextCleanup($text);

        if (strpos($text, '&#') === false) {
            return $nullLanguage;
        }

        // May want to run something here to convert stuff like &quot; to " or &alpha; to the NCR for the greek character for alpha
        $ncrs = $this->extractDecimalNcr($text);
        $hex = $this->extractHexNcr($text);
        $decimal_values = [];
        if (is_array($ncrs) && is_array($hex)) {
            $decimal_values = array_merge($ncrs, $hex);
        } elseif (is_array($ncrs)) {
            $decimal_values = $ncrs;
        } elseif (is_array($hex)) {
            $decimal_values = $hex;
        }

        // Detect language
        $detectedLanguages = [];
        foreach ($decimal_values as $val) {
            $language = $this->getLanguageForUtf8CodePoint($val);
            if ($language <> '') {
                if (!isset($detectedLanguages[$language])) {
                    $detectedLanguages[$language] = 1;
                } else {
                    $detectedLanguages[$language]++;
                }
            }
        }
        $result_count = 0;
        $result = '';
        foreach ($detectedLanguages as $key => $val) {
            if ($val > $result_count) {
                $result_count = $val;
                $result = $key;
            }
        }

        $totalCharacters = array_sum($detectedLanguages);
        $detectedLanguageCharacters = $result ? $detectedLanguages[$result] : 0;

        $result = trim(strtolower($result));
        $confidence = $totalCharacters > 0 ? $detectedLanguageCharacters / $totalCharacters : 0.0;

        if ($result === Language::LATIN_FAMILY || empty($result)) {
            return $nullLanguage;
        }

        if ($result == Language::CHINESE && isset($detectedLanguages[Language::KOREAN])) {
            $result = Language::KOREAN;
        }
        if ($result == Language::CHINESE && isset($detectedLanguages[Language::JAPANESE])) {
            $result = Language::JAPANESE;
        }
        if ($result == Language::ARABIC && isset($detectedLanguages[Language::FARSI])) {
            $result = Language::FARSI;
        }
        if ($result == Language::RUSSIAN && isset($detectedLanguages[Language::CHINESE])) {
            $result = Language::CHINESE;
        }

        return new Language($result, $confidence);
    }

    protected function extractDecimalNcr($text)
    {
        $matches = [];
        $result = false;
        $pattern = '/\&\#(\d+)\;/';        // &#nnn;
        preg_match_all($pattern, $text, $matches);
        if (is_array($matches)) {
            $result = $matches[1];
        }
        // Convert to integer
        if (is_array($result)) {
            foreach ($result as $key => $val) {
                $result[$key] = intval($val);
            }
        }

        return $result;
    }

    protected function extractHexNcr($text)
    {
        $result = [];
        $matches = [];
        $pattern = '/\&\#[xX]([0-9a-fA-F]+)\;/';        // &#xhhh; or &#Xhhh;
        preg_match_all($pattern, $text, $matches);
        if (is_array($matches)) {
            $result = $matches[1];
        }
        // Convert to decimal
        if (is_array($result)) {
            foreach ($result as $key => $val) {
                $result[$key] = intval(base_convert($val, 16, 10));
            }
        }

        return $result;
    }

    protected function latin1TextCleanup($string)
    {
        $string = str_replace('¡', '&#161;', $string);
        $string = preg_replace_callback(
            "/([\200-\237])/",
            function ($match) {
                return '&#'.(ord($match[1])).';';
            },
            $string
        );

        $string = preg_replace_callback(
            "/([\241-\377])/",
            function ($match) {
                return '&#'.(ord($match[1])).';';
            },
            $string
        );

        return $string;
    }

    protected function getLanguageForUtf8CodePoint($decimal)
    {
        // cjk = zh, ja, or ko.  Defaulting to zh since it's most common
        $decimal = intval($decimal);
        $detected_language = Language::LATIN_FAMILY;

        if ($decimal >= 880 && $decimal <= 1023) {
            $detected_language = Language::GREEK;
        } elseif ($decimal >= 1024 && $decimal <= 1279) {
            $detected_language = Language::RUSSIAN;
        } elseif ($decimal >= 1328 && $decimal <= 1423) {
            $detected_language = Language::ARMENIAN;
        } elseif ($decimal >= 1424 && $decimal <= 1535) {
            $detected_language = Language::HEBREW;
        } elseif ($decimal >= 1536 && $decimal <= 1791) {
            $detected_language = Language::ARABIC;
        } elseif ($decimal >= 3584 && $decimal <= 3711) {
            $detected_language = Language::THAI;
        } elseif ($decimal >= 6144 && $decimal <= 6319) {
            $detected_language = Language::MONGOLIAN;
        } elseif ($decimal >= 7936 && $decimal <= 8191) {
            $detected_language = Language::GREEK;           // greek extended
        } elseif ($decimal >= 11904 && $decimal <= 12031) {
            $detected_language = Language::CHINESE;         // CJK Radicals Supplement
        } elseif ($decimal >= 12032 && $decimal <= 12255) {
            $detected_language = Language::CHINESE;         // Kangxi Radicals

            // 0x2FF0 - 0x2FFF : Ideographic Description Characters (16)

        } elseif ($decimal >= 12288 && $decimal <= 12351) {
            $detected_language = Language::CHINESE;         // CJK Symbols and Punctuation
        } elseif ($decimal >= 12352 && $decimal <= 12447) {
            $detected_language = Language::JAPANESE;        // hirigana
        } elseif ($decimal >= 12448 && $decimal <= 12543) {
            $detected_language = Language::JAPANESE;        // katakana
            // What about kanji  & kana ???
        } elseif ($decimal >= 12544 && $decimal <= 12591) {
            $detected_language = Language::CHINESE;         // Bopomofo
        } elseif ($decimal >= 12592 && $decimal <= 12687) {
            $detected_language = Language::KOREAN;          // hangul compatibility jamo
        } elseif ($decimal >= 12688 && $decimal <= 12703) {
            $detected_language = Language::JAPANESE;        // Kanbun
        } elseif ($decimal >= 12704 && $decimal <= 12735) {
            $detected_language = Language::CHINESE;         // Bopomofo Extended
        } elseif ($decimal >= 12800 && $decimal <= 13055) {
            $detected_language = Language::CHINESE;         // Enclosed CJK Letters and Months
        } elseif ($decimal >= 13056 && $decimal <= 13311) {
            $detected_language = Language::CHINESE;         // CJK Compatibility
        } elseif ($decimal >= 13312 && $decimal <= 19893) {
            $detected_language = Language::CHINESE;         // CJK Unified Ideographs Extension A
        } elseif ($decimal >= 19968 && $decimal <= 40959) {
            $detected_language = Language::CHINESE;         // CJK Unified Ideographs
        } elseif ($decimal >= 40960 && $decimal <= 42127) {
            $detected_language = Language::CHINESE;         // Yi Syllables
        } elseif ($decimal >= 42128 && $decimal <= 42191) {
            $detected_language = Language::CHINESE;         // Yi Radicals
        } elseif ($decimal >= 44032 && $decimal <= 55203) {
            $detected_language = Language::KOREAN;          // Hangul Syllables
        } elseif ($decimal >= 63744 && $decimal <= 64255) {
            $detected_language = Language::CHINESE;         // CJK Compatibility Ideographs
        } elseif ($decimal >= 64336 && $decimal <= 65023) {
            $detected_language = Language::ARABIC;          // ar Presentation Forms-A
        } elseif ($decimal >= 65072 && $decimal <= 65103) {
            $detected_language = Language::CHINESE;         // CJK Compatibility Forms
        } elseif ($decimal >= 65136 && $decimal <= 65278) {
            $detected_language = Language::ARABIC;          // ar Presentation Forms-B
        }

        return $detected_language;
    }
}
