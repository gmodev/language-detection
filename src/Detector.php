<?php
namespace Gmo\LanguageDetection;

class Detector
{
    protected $nonLatinDetector;
    protected $latinDetector;

    public function __construct($apiPath = 'http://localhost:48000/')
    {
        $this->latinDetector = new LatinDetector($apiPath);
        $this->nonLatinDetector = new NonLatinDetector();
    }

    public function detect($text)
    {
        $nonLatin = $this->nonLatinDetector->detect($text);
        if ($nonLatin->getCode()) {
            return $nonLatin;
        }

        $latin = $this->latinDetector->detect($text);

        return $latin;
    }

    protected function detectLatin($text)
    {
        return $this->latinDetector->detect($text);
    }

    protected function detectNonLatin($text)
    {
        return $this->nonLatinDetector->detect($text);
    }
}
