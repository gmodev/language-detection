<?php
namespace Gmo\LanguageDetection;

class Language
{
    const ARABIC = 'ar';
    const ARMENIAN = 'hy';
    const CHINESE = 'zh';
    const DUTCH = 'nl';
    const ENGLISH = 'en';
    const FARSI = 'fa';
    const FRENCH = 'fr';
    const GERMAN = 'de';
    const GREEK = 'el';
    const HEBREW = 'he';
    const HINDI = 'hi';
    const ITALIAN = 'it';
    const INDONESIAN = 'id';
    const JAPANESE = 'ja';
    const KOREAN = 'ko';
    const MONGOLIAN = 'mn';
    const POLISH = 'po';
    const PORTUGUESE = 'pt';
    const ROMANIAN = 'ro';
    const RUSSIAN = 'ru';
    const SPANISH = 'es';
    const SWEDISH = 'sv';
    const THAI = 'th';
    const TURKISH = 'tr';
    const URDU = 'ur';
    const VIETNAMESE = 'vi';

    const LATIN_FAMILY = '';

    protected $code;
    protected $confidence;

    /**
     * @param string|null $code
     * @param float       $confidence
     */
    public function __construct($code, $confidence)
    {
        $this->code = $code ?: null;
        $this->confidence = $code ? $confidence : 0.0;
    }

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return float
     */
    public function getConfidence()
    {
        return $this->confidence;
    }
}
