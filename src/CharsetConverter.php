<?php
namespace Gmo\LanguageDetection;

class CharsetConverter
{
    public static function utf8ToNcr($text)
    {
        // 1-byte UTF-8 characters not in ISO-8859-1
        if (!preg_match("/[\200-\237]/e", $text) && !preg_match("/[\241-\377]/e", $text)) {
            return $text;
        }

        // 3-byte UTF-8 characters
        $text = preg_replace_callback(
            "/([\340-\357])([\200-\277])([\200-\277])/",
            function ($match) {
                return '&#'.((ord($match[1])-224)*4096 + (ord($match[2])-128)*64 + (ord($match[3])-128)).';';
            },
            $text
        );

        // 2-byte UTF-8 characters
        $text = preg_replace_callback(
            "/([\300-\337])([\200-\277])/",
            function ($match) {
                return '&#'.((ord($match[1])-192)*64+(ord('\\2')-128)).';';
            },
            $text
        );

        return $text;
    }
} 
