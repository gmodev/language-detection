#Language Detection#
A PHP library for detecting the language of a piece of text.  Both Latin and non-Latin languages are supported.  An ISO-639-1 language code and a % confidence are returned.  In the event that a language is unable to be detected, null is returned for the language code.

A Python-based service daemon must be installed in order to detect Latin languages.  The installer supports both SystemV and Upstart for running the daemon.

##Installation##
composer install
sudo vendor/bin/install-ldig.sh install

OR

composer install
sudo vendor/bin/install-ldig.sh install upstart

##Usage##
    $detector = GMO\LanguageDetection\Detector();
    $language = $detector->detect('Bienvenidos');
    $languageCode = $language->getCode();       // "es"
    $confidence = $language->getConfidence();       // 0.999999

##ldig##
Latin-based language detection is based on the ldig library.

I got the ldig code from:  https://github.com/shuyo/ldig
I created the scripts ldig/detect.py and ldig/detectServer.py based on the code in ldig/ldig.py and ldig/server.py .  These scripts are to make it more practical for everyday use

##TODO:##
* Add uninstaller for libs/ldig/install-ldig.sh
* Add more testcases
